#!/bin/bash

# Bash Script um User und Gruppen zu erstellen und anpassen
#
# Inhalt:
#1 Einlesen der Datei UserMusterGmbH.csv
#2 Kontrolle des Kurzzeichen
#3 User löschen falls gekündigt
#4 User erstellen oder von bestehnden Gruppen entfernen falls dieser existiert
#5 Gruppen prüfen / erstellen
#6 User Gruppe anpassen



#1**************************** Einlesen der Datei UserMusterGmbH.csv*******************************************************************
while IFS=";" read -r Namen Kurzzeichen Personalnummer Adresse PLZ Ort Geburtsdatum Abteilung Eintritt Austritt
do
now=$(date)                                                                                     # Setzen des Datum und Zeit zur Variable $now

  #2********************** Kontrolle des Kurzzeichen ******************************************************************************
  if [[ ${#Kurzzeichen} < 4 ]]
    then                                                                                        # wenn Kurzzeichen kleiner 4 ist, dann beende diesen User Loop
      echo "Kurzzeichen zu klein oder nicht vorhanden"
      echo >>Report.txt "ERROR: " $now " User:" $Namen ";" $Kurzzeichen " Kurzzeichen zu klein oder nicht vorhanden"
      continue                                                                                  #diesen User Loop beenden
    else
      echo "Kurzzeichen vorhanden: " $Kurzzeichen
  fi

  #3********************** User löschen falls gekündigt ***************************************************************************
  if [ -z $Austritt ]
    then                                                                                        # wenn Austritt ist "Zero-length", ist User nicht gekündet
      echo "User nicht gekündet "
    else                                                                                        # Austritt hat einen Wert, also wird der User gelöscht und vorher die Daten gesichert nach /home/EhemaligeMitarbeiter
      echo "User gekündigt am:" $Austritt
      mv /home/$Kurzzeichen /home/EhemaligeMitarbeiter
      userdel -r -f $Kurzzeichen
      echo >>Report.txt "INFO : " $now " User:" $Kurzzeichen " entfernt"
      continue                                                                                  # diesen User Loop beenden
  fi

  #4********************** User erstellen oder von bestehnden Gruppen entfernen falls dieser existiert *****************************
  useradd $Kurzzeichen  -m -p $(openssl passwd -1 password)
  if [ $? = 0 ]
    then                                                                                        # User erfoglreich erstellt. Der Befehl useradd hat kein Error "Return Code 0"
      usermod -aG Mitarbeiter $Kurzzeichen                                                      # User zur Gruppe Mitarbeiter hinzufügen
      chfn -o umask=177 $Kurzzeichen                                                            # umask anpaasen dass per default rw- --- --- / 600 verwendet wird
      mkdir /home/$Kurzzeichen/Arbeitsdokumente                                                 # Verzeichnis Arbeitsdokumente erstellen
      chown $Kurzzeichen:$Kurzzeichen /home/$Kurzzeichen/Arbeitsdokumente                       # Berechtigung für User anpassen
      mkdir /home/$Kurzzeichen/Bilder                                                           # Verzeichnis Arbeitsdokumente erstellen
      chown $Kurzzeichen:$Kurzzeichen /home/$Kurzzeichen/Bilder                                 # Berechtigung für User anpassen
      mkdir /home/$Kurzzeichen/Videos                                                           # Verzeichnis Arbeitsdokumente erstellen
      chown $Kurzzeichen:$Kurzzeichen /home/$Kurzzeichen/Videos                                 # Berechtigung für User anpassen
      mkdir /home/$Kurzzeichen/Archiv                                                           # Verzeichnis Arbeitsdokumente erstellen
      chown $Kurzzeichen:$Kurzzeichen /home/$Kurzzeichen/Archiv                                 # Berechtigung für User anpassen
      echo >>Report.txt "INFO : " $now " User:" $Kurzzeichen " erstellt. Passwort: password"
    else                                                                                        # User existiert bereits. Von allen Gruppen entfernen und weiter unten zur neuen Gruppe hinzufügen
      echo >>Report.txt "INFO : " $now " User:" $Kurzzeichen " existiert bereits"
      gpasswd -d $Kurzzeichen Geschäftsleitung
      gpasswd -d $Kurzzeichen Entwicklung
      gpasswd -d $Kurzzeichen Produktion
      gpasswd -d $Kurzzeichen Marketing
      gpasswd -d $Kurzzeichen Verkauf
      gpasswd -d $Kurzzeichen Personal
      gpasswd -d $Kurzzeichen Spedition
      gpasswd -d $Kurzzeichen Support
      gpasswd -d $Kurzzeichen IT
      gpasswd -d $Kurzzeichen Hausdienst
      echo >>Report.txt "INFO : " $now " User:" $Kurzzeichen " von allen Gruppen entfernt"
  fi

  #5********************** Gruppen prüfen / erstellen **************************************************************************
  groupadd $Abteilung
  if [ $? = 0 ]
    then                                                                                        # Gruppe erfoglreich erstellt. Der Befehl groupadd hat kein Error "Return Code 0"
      echo >>Report.txt "INFO : " $now " Abteilung :" $Abteilung" erstellt"
    else                                                                                        # Gruppe existiert bereits
      echo >>Report.txt "INFO : " $now " Abteilung :" $Abteilung" existiert bereits"
  fi

  #6********************** User Gruppe anpassen **********************************************************************************
  usermod -aG $Abteilung $Kurzzeichen
  if [ $? = 0 ]
    then                                                                                        # User erfoglreich angepasst. Der Befehl usermod hat kein Error "Return Code 0"
      echo >>Report.txt "INFO : " $now " User:" $Kurzzeichen " zur Gruppe: " $Abteilung " hinzugefügt"
    else                                                                                        # User existiert bereits in der Gruppe
      echo >>Report.txt "INFO : " $now " User:" $Kurzzeichen " existiert bereits in der Gruppe: " $Abteilung
  fi

done < UserMusterGmbH.csv