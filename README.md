# m122-Bashprojekt






<details> 

## Der Script ist dient zur automatisierten Bewirtschaftung der LinuxUseraccounts in der Firma Muster GmbH.

Der Script enthält sechs Funktionen

1 Einlesen der Datei UserMusterGmbH.csv

2 Kontrolle des Kurzzeichen

3 User löschen falls gekündigt

4 User erstellen oder von bestehnden Gruppen entfernen falls dieser existiert

5 Gruppen prüfen / erstellen

6 User Gruppe anpassen

Damit diese funktionen richtig ausgeführt werden, muss im selben Verzeichnis eine folgende CSV vorhanden sein:
UserMusterGmbH.csv (siehe CSV Datei in Git Repo)

Diese Datei enthält alle User Informationen die der Bash Script benötigt.

Alle Infos oder Errors die der Script erzeugt, werden in einer Datei Report.txt ausgegeben.
Der Inhalt dieser Datei dient auch dem Error-Reporting  und dem wöchentlichem Reporting der Accounting-History zuhanden Personalbüro.


## [Script für Usererstellung](https://gitlab.com/18remo96/m122-bashprojekt/-/blob/main/UserMusterGmbH.sh)

<summary> 

# Unser Bash-Script

</summary>

</details>
 




<details> 

### Wir haben das Script getestet und ein Testprotokoll erstellt, im gleichen File findet man auch die dazugehörige Dokumentation

[Dokumentation und Testprotokoll](https://gitlab.com/18remo96/m122-bashprojekt/-/blob/main/UserMusterGmbH-Doku.xlsx)

<summary> 

## Testprotokoll und Dokumentation

</summary>

</details>




<details> 

Der Bash Script benötigt keine Installation. Die Dateien

-  UserMusterGmbH.sh
-  UserMusterGmbH.csv

können in ein beliebiges Verzeichnis erstellt/kopiert werden.


Der Datei UserMusterGmbH.sh muss noch das execute Attribut gegeben werden. 

Um User und Gruppen zu bearbeiten wird erst das CSV bearbeitet und dann einfach das Script ./UserMusterGmbH.sh ausgefürt. 

Resultate, Infos, Errors und History sind in der Datei Report.txt ersichtlich. 

<summary> 

## Installation und Betrieb

</summary>

</details>





<details> 

### Um die User/Gruppen zu erstellen/ändern/löschen, haben wir eine CSV Datei erstellt. 

[CSV-Datei](https://gitlab.com/18remo96/m122-bashprojekt/-/blob/main/UserMusterGmbH.csv)

<summary> 

## CSV Datei

</summary>

</details>







 